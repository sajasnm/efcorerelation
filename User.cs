namespace EfCoreRelation
{
    public class User
    {
        public int Id { get; set; }
        public string Username { get; set; } = string.Empty;

        public List<Character> characters { get; set; }
    }
}