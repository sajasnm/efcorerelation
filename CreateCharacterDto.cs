
namespace EfCoreRelation
{
    public class CreateCharacterDto
    {

        public string Name { get; set; } = "Frodo";
        public string RpgClass { get; set; } = "Knight";
        public int UserId { get; set; } = 1;

    }
}