using Microsoft.AspNetCore.Mvc;

namespace EfCoreRelation.Controllers;

[ApiController]
[Route("[controller]")]
public class CharacterController : ControllerBase
{



    private readonly DataContext _context;
    public CharacterController(DataContext dataContext)
    {
        _context = dataContext;
    }

    [HttpGet]
    public async Task<ActionResult<List<Character>>> Get(int userId)
    {
        var characters = await _context.Characters.Where(c => c.UserId == userId).Include(c => c.Weapon).Include(c => c.Skils).ToListAsync();
        return characters;

    }

    [HttpPost]
    public async Task<ActionResult<List<Character>>> Create(CreateCharacterDto req)
    {

        var user = await _context.Users.FindAsync(req.UserId);

        if (user == null)
        {
            return NotFound();
        }
        var newCharacter = new Character
        {
            Name = req.Name,

            RpgClass = req.RpgClass,

            User = user
        };

        _context.Characters.Add(newCharacter);
        await _context.SaveChangesAsync();
        return await Get(newCharacter.UserId);
    }


    [HttpPost("weapon")]
    public async Task<ActionResult<Character>> CreateWeapon(CreateWeaponDto req)
    {

        var character = await _context.Characters.FindAsync(req.CharacterId);

        if (character == null)
        {
            return NotFound();
        }
        var newWeapon = new Weapon
        {
            Name = req.Name,

            Damage = req.Damage,

            Character = character
        };

        _context.Weapons.Add(newWeapon);
        await _context.SaveChangesAsync();
        return character;
    }

    [HttpPost("skill")]
    public async Task<ActionResult<Character>> AddCharecterSkil(CreateCharecterSkillDto req)
    {

        var character = await _context.Characters.Where(c => c.Id == req.CharacterId).Include(c => c.Skils).FirstOrDefaultAsync();

        if (character == null)
        {
            return NotFound();
        }

        var skill = await _context.Skils.FindAsync(req.SkillId);

        if (skill == null)
        {
            return NotFound();
        }
        character.Skils.Add(skill);

        await _context.SaveChangesAsync();
        return character;
    }
}
