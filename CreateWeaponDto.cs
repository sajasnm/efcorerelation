
namespace EfCoreRelation
{
    public class CreateWeaponDto
    {

        public string Name { get; set; } = "Knife";
        public int Damage { get; set; } = 10;
        public int CharacterId { get; set; } = 1;

    }
}